package main

import (
	"fmt"
	"log"
	"os"

	"github.com/xuperchain/xuper-sdk-go/account"
	"github.com/xuperchain/xuper-sdk-go/contract"
)

var (
	node   = "39.156.69.83:37100"
	bcname = "xuper"
)

func usingAccount() (*account.Account, error) {
	// 需要将下载的超级链private.key私钥放入↓路径，第二个参数为6位交易密码
	acc, _ := account.GetAccountFromFile("./data/keys/", "******")
	return acc, nil
}

// 上链调用
func useInvokeWasmContract(methodName string, args map[string]string) {
	acc, _ := usingAccount()
	// 合约账户地址
	contractAccount := "XC****************@xuper"
	// 合约名称
	contractName := "pingpong01"
	wasmContract := contract.InitWasmContract(acc, node, bcname, contractName, contractAccount)

	txid, err := wasmContract.InvokeWasmContract(methodName, args)
	if err != nil {
		fmt.Printf("InvokeWasmContract err: %v\n\n", err)
		os.Exit(-1)
	}
	fmt.Printf("txid: %v\n\n", txid)
	return
}

// 不上链查询
func useQueryWasmContract(methodName string, args map[string]string) {
	acc, _ := usingAccount()
	contractAccount := "XC****************@xuper"
	contractName := "pingpong01"
	wasmContract := contract.InitWasmContract(acc, node, bcname, contractName, contractAccount)
	preExeRPCRes, err := wasmContract.QueryWasmContract(methodName, args)
	if err != nil {
		log.Printf("QueryWasmContract failed, err: %v", err)
		os.Exit(-1)
	}
	gas := preExeRPCRes.GetResponse().GetGasUsed()
	fmt.Printf("gas used: %v\n", gas)
	for _, res := range preExeRPCRes.GetResponse().GetResponse() {
		fmt.Printf("contract response: %s\n", string(res))
	}
}

// 调用addUser添加用户
func addUser_SC(userid string, username string) {
	args := map[string]string{
		"userid":   userid,
		"username": username,
	}
	useInvokeWasmContract("addUser", args)
	return
}

// 调用queryPingPongByUser根据用户查询拥有的乒乓球拍
func queryPingPongByUser_SC(userid string) {
	args := map[string]string{
		"userid": userid,
	}
	useQueryWasmContract("queryPingPongByUser", args)
	return
}

//调用bindUserAndPingPong用户——乒乓球拍信息绑定
func bindUserAndPingPong_SC(userid string, pingpongid string) {
	args := map[string]string{
		"userid":     userid,
		"pingpongid": pingpongid,
	}
	useInvokeWasmContract("bindUserAndPingPong", args)
	return
}

// 调用traceBackPingPong乒乓球拍信息溯源
func traceBackPingPong_SC(pingpongid string) {
	args := map[string]string{
		"pingpongid": pingpongid,
	}
	useQueryWasmContract("traceBackPingPong", args)
	return
}

// 调用forgeryPingPong乒乓球拍鉴伪
func forgeryPingPong_SC(userid string, pingpongid string) {
	args := map[string]string{
		"userid":     userid,
		"pingpongid": pingpongid,
	}
	useQueryWasmContract("forgeryPingPong", args)
	return
}

// 调用queryOwner查看权限用户
func queryOwner() {
	args := map[string]string{}
	useQueryWasmContract("queryOwner", args)
}

func main() {
	// 添加用户记录，参数：用户id，用户名
	// addUser_SC("004", "lvlinfeng")

	// 查询用户名下乒乓球拍，参数：用户id
	// queryPingPongByUser_SC("001")

	// 用户和乒乓球绑定，参数：用户id，乒乓球拍id
	// bindUserAndPingPong_SC("001", "4")

	// 乒乓球拍溯源所有交易过程，参数：乒乓球拍id
	traceBackPingPong_SC("1")

	// 乒乓球拍和所有人是否对应验证，参数：用户id，乒乓球id
	// forgeryPingPong_SC("001", "1")

	// 查询可写入权限超级链地址
	queryOwner()
	return
}
